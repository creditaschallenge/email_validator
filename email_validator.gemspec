Gem::Specification.new do |spec|
  spec.name          = "email_validator"
  spec.version       = "0.0.2"
  spec.authors       = ["Bankfacil", "Raphael Monteiro, Kleber Nascimento"]
  spec.email         = ["dev@bankfacil.com.br", "rmonteiro89@hotmail.com, kleberng1990@gmail.com"]
  spec.homepage      = "https://github.com/BankFacil/email_validator"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]
  spec.summary       = %q{An email validator for Rails.}

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.add_dependency "activemodel", "~> 4.0"
end
