require "email_validator/version"
require 'active_model'

class EmailValidator < ActiveModel::EachValidator
  def validate_each(object, attribute, value)
    unless value =~ (/\A\w(\w|[.-]\w)*@\w((\w|[-]\w)*\.\w+)+\z/i)
        object.errors[attribute] << (options[:message] || 'não é válido')
    end
  end
end
