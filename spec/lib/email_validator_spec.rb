require 'spec_helper'
require 'email_validator'

describe EmailValidator do

  before(:each) do
    @validator = EmailValidator.new({attributes: ['bla']})
    @model_to_validate = double('model')

    model_errors = double('model_errors')
    allow(@model_to_validate).to receive("errors").and_return(model_errors)

    errors_array = double('errors_array')
    allow(model_errors).to receive('[]').and_return(errors_array)

    # allow(@model_to_validate.errors[]).to receive('<<')
  end

  it "should validate valid address" do
    expect(@model_to_validate.errors[]).not_to receive('<<')    
    @validator.validate_each(@model_to_validate, "email", "test@test.com")
  end

  context 'invalid email'

  it "should validate invalid address" do
    expect(@model_to_validate.errors[]).to receive('<<')
    @validator.validate_each(@model_to_validate, "email", "notvalid")
  end 

  it 'an @ character must separate the local and domain parts' do
    expect(@model_to_validate.errors[]).to receive('<<')
    @validator.validate_each(@model_to_validate, 'email', 'Abc.example.com')
  end

  it 'only one @ is allowed' do
    expect(@model_to_validate.errors[]).to receive('<<')
    @validator.validate_each(@model_to_validate, 'email', 'A@b@c@example.com')
  end

  it 'none of the special characters in this local part is allowed' do
    expect(@model_to_validate.errors[]).to receive('<<')
    @validator.validate_each(@model_to_validate, 'email', 'a"b(c)d,e:f;g<h>i[j\k]l@example.com')
  end

  it 'quoted strings must be dot separated or the only element making up the local-part' do
    expect(@model_to_validate.errors[]).to receive('<<')
    @validator.validate_each(@model_to_validate, 'email', 'just"not"right@example.com')
  end

  it 'does not accept spaces, quotes, and backslashes' do
    expect(@model_to_validate.errors[]).to receive('<<')
    @validator.validate_each(@model_to_validate, 'email', 'this is"not\allowed@example.com')
  end

  it 'even if escaped (preceded by a backslash), spaces, quotes, and backslashes must still be contained by quotes' do
    expect(@model_to_validate.errors[]).to receive('<<')
    @validator.validate_each(@model_to_validate, 'email', 'this\ still\"not\\allowed@example.com')
  end

  it 'does not accept double dot before @' do
    expect(@model_to_validate.errors[]).to receive('<<')
    @validator.validate_each(@model_to_validate, 'email', 'john..doe@example.com')
  end

  it 'does not accept double dot after @' do
    expect(@model_to_validate.errors[]).to receive('<<')
    @validator.validate_each(@model_to_validate, 'email', 'john.doe@example..com')
  end

  it 'does not accept accent marks' do
    expect(@model_to_validate.errors[]).to receive('<<')
    @validator.validate_each(@model_to_validate, 'email', 'claúdia@hotmail.com')
  end
end