# EmailValidator

This gem is a simple email validator, and it has the following rules:
- an @ character must separate the local and domain parts
- only one @ is allowed
- none of the special characters in this local part is allowed
- quoted strings must be dot separated or the only element making up the local-part
- does not accept spaces, quotes, and backslashes
- does not accept even if escaped (preceded by a backslash), spaces, quotes, and backslashes
- does not accept double dot before @
- does not accept double dot after @
- does not accept accent marks

Regex to validate email: /\A\w(\w|[.-]\w)*@\w((\w|[-]\w)*\.\w+)+\z/i

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'email_validator'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install email_validator

## Using it

This gem is a simple `ActiveModel::EachValidator` and you can use it like:

`validates <field-to-be-validated>, email: true`

Ex.
```ruby
validates :spouse_email, email: true
```

## Contributing

1. Fork it ( https://github.com/[my-github-username]/email_validator/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
